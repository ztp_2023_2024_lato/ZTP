import json
import pytest
from website.models import Course, db

def test_signUp(client):
    response = client.post('/sign_up', data={
        "email": "test@test.pl",
        "first_name": "Testing",
        "password1": "password1",
        "password2": "password1",
    }, follow_redirects=True)

    assert response.status_code == 200

def test_login(client):
    response = client.post('/login', data={
        "email":"test@test.pl",
        "password":"password1"
    }, follow_redirects=True)

    assert response.status_code == 200

def test_auth_unknown_user(client):
    response = client.post('/login', data={
        "email": "",
        "password": ""
    })

    assert b'Email does not exist.' in response.data

def test_create_note_authorised(client):
    response = client.post('/login', data={
        "email":"test@test.pl",
        "password":"password1"
    }, follow_redirects=True)

    assert response.status_code == 200

    response = client.post('/notes', data={
        "note": "some random text"
    })

    assert b'some random text' in response.data

def test_create_note_unauthorised(client):
    response = client.post('/notes', data={
        "note": "some random text"
    })

    # Redirection to login page
    assert response.status_code == 302

def test_get_courses_no_data(client):
    response = client.get('/courses', json={})
    assert response.status_code == 200
    assert response.json == {"courses": []}

def test_get_courses_with_data(client, app, sample_course):
    with app.app_context():
        db.session.add(sample_course)
        db.session.commit()
    
    response = client.get('/courses', json={"search_value": "Sample"})
    assert response.status_code == 200
    assert len(response.json["courses"]) == 1
    assert response.json["courses"][0]["title"] == "Sample Course"

def test_post_course_success(client):
    response = client.post('/courses', json={
        "name": "New Course",
        "description": "A new course description",
        "price": 99.99,
        "category": "New Category"
    })
    assert response.status_code == 201
    assert response.json["success"] is True
    assert response.json["message"] == "Course added successfully"

def test_post_course_missing_fields(client):
    response = client.post('/courses', json={
        "name": "Incomplete Course"
        # Missing description, price, category
    })
    assert response.status_code == 400
    assert response.json["message"] == "All fields are required"

def test_post_course_duplicate_title(client, app, sample_course):
    with app.app_context():
        db.session.add(sample_course)
        db.session.commit()
    
    response = client.post('/courses', json={
        "name": "Sample Course",  # Duplicate title
        "description": "Another description",
        "price": 120.00,
        "category": "Another Category"
    })
    assert response.status_code == 400
    assert response.json["success"] is False
    assert response.json["message"] == "Course title must be unique"


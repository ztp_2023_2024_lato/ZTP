from flask import Blueprint, render_template, request, flash, jsonify
from flask_login import login_required, current_user
from .models import Note, Course
from sqlalchemy.exc import IntegrityError
from . import db
import json

views = Blueprint('views', __name__)

@views.route('/', methods=['GET', 'POST'])
#@login_required
def home():
    if request.method == 'POST': 
        data = request.get_json()
        note = data.get('note')
        if len(note) < 1:
            flash('Note is too short!', category='error') 
        else:
            new_note = Note(data=note, user_id=current_user.id)  #schemat dla notatki
            db.session.add(new_note) #dodowanie notatki
            db.session.commit()
            flash('Note added!', category='success')

    return render_template("home.html", user=current_user)

@views.route('/notes', methods=['GET', 'POST'])
@login_required
def notes():
    if request.method == 'POST': 
        note = request.form.get('note')#Gets the note from the HTML 

        if len(note) < 1:
            flash('Note is too short!', category='error') 
        else:
            new_note = Note(data=note, user_id=current_user.id)  #schemat dla notatki
            db.session.add(new_note) #dodowanie notatki
            db.session.commit()
            flash('Note added!', category='success')

    return render_template("notes.html", user=current_user)

@views.route('/courses', methods=['GET', 'POST'])
#@login_required
def courses():
    if request.method == 'GET':
        data = request.get_json()
        search_value = data.get('search_value', '')

        matching_courses = Course.query.filter(Course.title.ilike(f'%{search_value}%')).all()

        courses_list = [
            {
                'id': course.id,
                'course_date': course.course_date.isoformat(),  # Convert datetime to ISO format string
                'title': course.title,
                'description': course.description,
                'user_id': course.user_id
            }
            for course in matching_courses
        ]
        print("Wykonuje się to ")
        return jsonify(courses=courses_list)
    
    elif request.method == 'POST':
        data = request.get_json()
        print(data)
        title = data.get('name')
        description = data.get('description')
        price = data.get('price')
        category = data.get('category')
        #image = request.files.get('image')  # Assuming the file field is named 'image'

        if not (title and description and price and category):
            return jsonify(message="All fields are required"), 400

        try:
            new_course = Course(
                title=title,
                description=description,
                price=price,
                category=category,
            )

            db.session.add(new_course)
            db.session.commit()
            # Save the image to the server (if needed)
            #image_path = f"path/to/save/{image.filename}"
            #image.save(image_path)
            return jsonify(success=True, message="Course added successfully"), 201
        except IntegrityError:
            db.session.rollback()
            return jsonify(success=False, message="Course title must be unique"), 400
        except Exception as e:
            db.session.rollback()
            print(e)
            return jsonify(success=False, message=str(e)), 500
    
    return jsonify(message="Method not allowed"), 405

@views.route('/courses/<id>', methods=['GET', 'POST'])
#@token_required
def get_one_courses(id):#current_user, 
    
    if request.method == 'GET':
        courses = Course.query.filter_by(id=id).first()#, user_id=current_user.id

        if not courses:
            return jsonify({'message' : 'No courses found!'})

        courses_data = {}
        courses_data['id'] = courses.id
        courses_data['description'] = courses.description
        courses_data['title'] = courses.title
        courses_data['complete'] = courses.complete
        
    if request.method == 'POST':
        data = request.get_json()
        course_id = data.get('course_id')
        description = data.get('description')
        title = data.get('title')
        complete = data.get('complete')

        new_course = Course(id=course_id, description=description, title=title, complete=complete)
        db.session.add(new_course)
        db.session.commit()

        return jsonify({"message": "Course added"}), 200
    else:
        return jsonify({"message": "Fail"}), 400

@views.route('/courses/<id>', methods=['DELETE'])
#@token_required
def delete_courses(courses_id):#,current_user, 
    courses = Course.query.filter_by(id=id).first()#, , user_id=current_user.id

    if not courses:
        return jsonify({'message' : 'No courses found!'})

    db.session.delete(courses)
    db.session.commit()

    return jsonify({'message' : 'courses item deleted!'})


@views.route('/administration_panel', methods=['GET'])
@login_required
def administration_panel():
    
    return render_template("administration_panel.html", user=current_user)

@views.route('/delete_note', methods=['POST'])
def delete_note():  
    note = json.loads(request.data)
    noteId = note['noteId']
    note = Note.query.get(noteId)
    if note:
        if note.user_id == current_user.id:
            db.session.delete(note)
            db.session.commit()

    return jsonify({})

@views.route('/instructor', methods=['POST'])
def add_course():
    if request.method == 'POST':
        data = request.get_json()
        course_id = data.get('course_id')
        description = data.get('description')
        title = data.get('title')
        complete = data.get('complete')

        new_course = Course(id=course_id, description=description, title=title, complete=complete)
        db.session.add(new_course)
        db.session.commit()

        return jsonify({"message": "Course added"}), 200
    else:
        return jsonify({"message": "Fail"}), 400
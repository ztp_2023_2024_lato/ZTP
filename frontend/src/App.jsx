import React, { useState } from 'react'
import Navbar from './components/Navbar/Navbar'
import { Route, Routes } from 'react-router-dom'
import Home from './pages/Home/Home'
import Cart from './pages/Cart/Cart'
import PlaceOrder from './pages/PlaceOrder/PlaceOrder'
import LogIn from './components/LogIn/LogIn'
import axios from 'axios'
import Instructor from './pages/Instructor/Instructor'
import Account from './pages/Account/Account'
import MyCourses from './pages/MyCourses/MyCourses'
import Footer from './components/Footer/Footer'

const App = () => {

  const[showLogin,setShowLogin] = useState(false)

    const fetch_API = async () => {
      const response = await axios.get(`${url}`)
    }

  return (
    <>
    {showLogin?<LogIn setShowLogin={setShowLogin}/>:<></>}
    <div className='app'>
      <Navbar setShowLogin={setShowLogin}/>
      <Routes>
        <Route path='/' element={<Home/>}></Route>
        <Route path='/cart' element={<Cart/>}></Route>
        <Route path='/order' element={<PlaceOrder/>}></Route>
        <Route path='/instructor' element={<Instructor/>}></Route>
        <Route path='/my-account' element={<Account/>}></Route>
        <Route path='/my-courses' element={<MyCourses/>}></Route>
      </Routes>
    </div>
    <Footer></Footer>
    </>
  )
}

export default App

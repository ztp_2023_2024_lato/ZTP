import React, { useContext } from 'react'
import './CourseDisplay.css'
import { StoreContext } from '../../context/StoreContext'
import CourseItem from '../CourseItem/CourseItem'

const CourseDisplay = ({category}) => {

    const {food_list} = useContext(StoreContext)

  return (
    <div className='course-display' id='course-display'>
      <h2>TEXT!</h2>
      <div className="course-display-list">
        {food_list.map((item,index)=>{
            if(category==="All" || category===item.category){
                return <CourseItem key={index} id={item._id} name={item.name} description={item.description} price={item.price} image={item.image}/>
            }
        })}
      </div>
    </div>
  )
}

export default CourseDisplay

import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom'; // Import for extended matchers
import { BrowserRouter as Router } from 'react-router-dom';
import Navbar from './Navbar';
import { StoreContext } from '../../context/StoreContext';

describe('Navbar Component', () => {
  test('renders Navbar correctly', () => {
    // Check if the logo image is rendered with the correct alt text
    expect(screen.getByAltText('Logo')).toBeInTheDocument();

    // Check if menu links are rendered
    expect(screen.getByText('Home')).toBeInTheDocument();
    expect(screen.getByText('Courses')).toBeInTheDocument();
    expect(screen.getByText('Mobile App')).toBeInTheDocument();
    expect(screen.getByText('Contact Us')).toBeInTheDocument();

    // Check if search icon is rendered
    expect(screen.getByAltText('Search Icon')).toBeInTheDocument();
  });

  test('shows login button when user is not authenticated', () => {
    // Mock context values with empty token
    const mockContextValue = {
      getTotalCartAmount: jest.fn(),
      token: '',
      setToken: jest.fn(),
    };

    render(
      <Router>
        <StoreContext.Provider value={mockContextValue}>
          <Navbar />
        </StoreContext.Provider>
      </Router>
    );

    // Check if login button is rendered
    expect(screen.getByText('Sing In')).toBeInTheDocument();
  });
  test('displays user profile icon when logged in', () => {
    // Mocking logged-in state
    const mockToken = 'mockToken';
    jest.spyOn(React, 'useContext').mockReturnValue({
      token: mockToken
    });

    render(<Navbar />);

    // Check if user profile icon is rendered
    expect(screen.getByAltText('User Profile')).toBeInTheDocument();
  });
});

import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ExploreMenu from './ExploreMenu';
import { menu_list } from '../../assets/assets';

describe('ExploreMenu Component', () => {
  test('renders ExploreMenu correctly', () => {
    const mockCategory = 'Coding';
    const mockSetCategory = jest.fn();

    render(<ExploreMenu category={mockCategory} setCategory={mockSetCategory} />);

    // Check if heading is rendered
    expect(screen.getByText('Explore our courses')).toBeInTheDocument();

    // Check if menu items are rendered
    menu_list.forEach((menuItem) => {
      expect(screen.getByText(menuItem.menu_name)).toBeInTheDocument();
      expect(screen.getByAltText(`${menuItem.menu_name} Image`)).toBeInTheDocument();
    });

    // Check if active class is applied to the correct menu item
    expect(screen.getByText(mockCategory)).toHaveClass('active');
  });

  test('calls setCategory with correct value when menu item is clicked', () => {
    const mockCategory = 'Coding';
    const mockSetCategory = jest.fn();

    render(<ExploreMenu category={mockCategory} setCategory={mockSetCategory} />);

    // Simulate click on menu item
    fireEvent.click(screen.getByText('Math'));

    // Check if setCategory is called with correct value
    expect(mockSetCategory).toHaveBeenCalledWith('Math');
  });
});

import React, { useContext, useState } from 'react'
import './CourseItem.css'
import { assets } from '../../assets/assets'
import { StoreContext } from '../../context/StoreContext'

const CourseItem = ({id,name,price,description,image}) => {

    const {cartItems, addToCart, removeFromCart} = useContext(StoreContext);

  return (
    <div className='course-item'>
        <div className="course-item-image-container">
            <img className="course-item-image" src={image} alt="" />
            {!cartItems[id]
                ?<img className='add' onClick={()=>addToCart(id)} src={assets.add_icon_white} alt=""/>
                :<div className='course-item-counter'>
                    <img onClick={()=>removeFromCart(id)} src={assets.remove_icon_red} alt="" />
                    <p>{cartItems[id]}</p>
                    <img onClick={()=>addToCart(id)} src={assets.add_icon_green} alt="" />
                    </div>  
            }
        </div>
      <div className="course-item-info">
        <div className="course-item-name-raiting">
            <p>{name}</p>
            <img src={assets.rating_starts} alt="" />
        </div>
        <p className="course-item-description">{description}</p>
        <p className="course-item-proce">{price}</p>
      </div>
    </div>
  )
}

export default CourseItem

import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import LogIn from './LogIn';
import { StoreContext } from '../../context/StoreContext';
import { BrowserRouter as Router } from 'react-router-dom';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';

jest.mock('axios');

const mockSetShowLogin = jest.fn();
const mockSetToken = jest.fn();
const mockUrl = "http://127.0.0.1:5000";

const renderLogIn = () => {
  return render(
    <StoreContext.Provider value={{ url: mockUrl, setToken: mockSetToken }}>
      <Router>
        <LogIn setShowLogin={mockSetShowLogin} />
        <ToastContainer />
      </Router>
    </StoreContext.Provider>
  );
};

describe('LogIn Component', () => {
    test('renders LogIn form', () => {
        const { getByRole } = render(<LogIn />);
        const loginForm = getByRole('form', { name: /LogIn/i });
        expect(loginForm).toBeInTheDocument();
    });

    // test('switches to Sign Up form', () => {
    //     renderLogIn();

    //     fireEvent.click(screen.getByText(/Create account/i));
    //     expect(screen.getByPlaceholderText(/Name/i)).toBeInTheDocument();
    //     expect(screen.getByText(/Create account/i)).toBeInTheDocument();
    // });

    // test('shows alert when role is not selected in Sign Up', async () => {
    //     renderLogIn();

    //     fireEvent.click(screen.getByText(/Create account/i));
    //     fireEvent.change(screen.getByPlaceholderText(/Name/i), { target: { value: 'Test User' } });
    //     fireEvent.change(screen.getByPlaceholderText(/Email/i), { target: { value: 'test@test.com' } });
    //     fireEvent.change(screen.getByPlaceholderText(/Password/i), { target: { value: 'password' } });

    //     fireEvent.click(screen.getByText(/Create account/i));

    //     expect(await screen.findByText(/Please select a role./i)).toBeInTheDocument();
    // });

    // test('handles successful login', async () => {
    //     axios.post.mockResolvedValueOnce({ data: { token: 'test-token', message: 'Login successful' }, status: 200 });

    //     renderLogIn();

    //     fireEvent.change(screen.getByPlaceholderText(/Email/i), { target: { value: 'test@test.com' } });
    //     fireEvent.change(screen.getByPlaceholderText(/Password/i), { target: { value: 'password' } });

    //     fireEvent.click(screen.getByText(/LogIn/i));

    //     expect(await screen.findByText(/Login successful/i)).toBeInTheDocument();
    //     expect(mockSetToken).toHaveBeenCalledWith('test-token');
    //     expect(mockSetShowLogin).toHaveBeenCalledWith(false);
    // });

    // test('handles login error', async () => {
    //     axios.post.mockRejectedValueOnce(new Error('Login failed'));

    //     renderLogIn();

    //     fireEvent.change(screen.getByPlaceholderText(/Email/i), { target: { value: 'test@test.com' } });
    //     fireEvent.change(screen.getByPlaceholderText(/Password/i), { target: { value: 'wrongpassword' } });

    //     fireEvent.click(screen.getByText(/LogIn/i));

    //     expect(await screen.findByText(/Wrong credentials/i)).toBeInTheDocument();
    // });
});

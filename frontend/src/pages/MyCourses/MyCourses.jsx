import React, { useEffect, useState } from 'react'
import "./MyCourses.css"
import axios from 'axios'
import {toast} from 'react-toastify'

const MyCourses = () => {

  const url = "http://127.0.0.1:5000";
  const [list, setList] = useState([]);

  const fetchList = async () => {
    const response = await axios.get(url + "/courses");
    if(response.data.success){
      setList(response.data.data);
    }
    else{
      toast.error('Error lol')
    }
  }

  const removeCourse = async (courseId) => {
    const response = await axios.delete(`${url}/courses/`,{id:courseId});
    await fetchList();
  }
  useEffect(()=>{
    fetchList();
  },[])

  return (
    <div className='list add flex-col'>
      <p>All My Courses</p>
    <div className="list-table">
      <div className="list-table-format title">
    <b>Image</b>
    <b>Name</b>
    <b>Category</b>
    <b>Price</b>
    <b>Action</b>
      </div>
      {list.map((item, index)=>{
        return (
          <div key={index} className='list-table-format'>
            <img src={`${url}/images`+item.image} alt="" />
            <p>{item.name}</p>
            <p>{item.category}</p>
            <p>${item.price}</p>
            <p onClick={()=>removeCourse(item._id)} className='cursor'>X</p>
          </div>
        )

      })}
    </div>
      
    </div>
  )
}

export default MyCourses
